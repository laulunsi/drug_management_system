# 药品管理系统

#### Description
需求分析
这个系统是帮助医护人员完成日常繁重窗口业务的工具。借助计算机系统，使他们凌乱的工作变得有条理，解脱他们需要记忆大量信息（药品的规格,价钱,疾病的名称与编码等）的困难。保证他们遵守某些规范，减轻他们汇总、统计、报告和传递这些信息的负担。因此，尽量符合这些事务处理级工作人员的工作秩序与工作习惯，功能完整，操作简单，响应迅速，界面友善，易学易用成为这类软件必须满足的功能要求。 
管理员登录以后可以很清晰的看到各种项目列表和可以实现的功能，库存管理和药品管理一目了然。由于是限定医院使用，所有只有登录系统。库存管理功能是在使用系统的开始初始化医院的库存，并进行库存管理的其他操作，可以修改库存，读取。这个药品管理系统主要涉及医院药库的药品进、出、存等业务，以及入库、出库和库存管理，药品管理主要管理药库中所有药品的进出和内部统计计算，为药品会计提供基础数据，以及包括有效期的报警和下限报警。

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)