﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient; 
using System.Windows.Forms;

namespace 药品管理系统
{
    public partial class frm_Login : Form
    {
        public frm_Login()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen; 
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void 医院药品管理系统_Click(object sender, EventArgs e)
        {

        }

        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = new SqlConnection();                              
            sqlConnection.ConnectionString =
                "Server=LAPTOP-TGDPSLHN;Database=MMS;Integrated Security=sspi";            
            SqlCommand sqlCommand = new SqlCommand();                                      
            sqlCommand.Connection = sqlConnection;                                         
            sqlCommand.CommandText = "SELECT COUNT(1) FROM tb_user"                                                       
                + " WHERE Uno='" + this.txb_UserNo.Text.Trim() + "'"                         
                + " AND Upassword=HASHBYTES('MD5','" + this.txb_Password.Text.Trim() + "');";           
            sqlConnection.Open();                                                           
            int rowCount = (int)sqlCommand.ExecuteScalar();                                 
                                                                                            
            sqlConnection.Close();                                                        
            if (rowCount == 1)                                                              
            {
                MessageBox.Show("登录成功。");                                              
            }
            else                                                                           
            {
                MessageBox.Show("用户号/密码有误，请重新输入！");                           
                this.txb_Password.Focus();                                                 
                this.txb_Password.SelectAll();                                             
            }
            frm_Menu a = new frm_Menu();
            a.Show();
            this.Hide();
        }

        private void btn_Register_Click(object sender, EventArgs e)
        {
            frm_Register f = new frm_Register();
            f.Show();
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.txb_UserNo.Text = "";
            this.txb_Password.Text = "";

        }

        }
    }

